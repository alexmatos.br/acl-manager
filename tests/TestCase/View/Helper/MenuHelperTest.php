<?php
namespace AclManager\Test\TestCase\View\Helper;

use AclManager\View\Helper\MenuHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * AclManager\View\Helper\MenuHelper Test Case
 */
class MenuHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \AclManager\View\Helper\MenuHelper
     */
    public $Menu;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Menu = new MenuHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Menu);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

<?php
namespace AclManager\Test\TestCase\View\Helper;

use AclManager\View\Helper\TesteHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * AclManager\View\Helper\TesteHelper Test Case
 */
class TesteHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \AclManager\View\Helper\TesteHelper
     */
    public $Teste;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Teste = new TesteHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Teste);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

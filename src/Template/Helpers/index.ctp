<?php
$this->PluginPageHeader->addTitle(['label' => 'Helpers', 'icon' => 'fa fa-question-circle']);
$this->PluginPageHeader->addSubTitle(['label' => 'List Helper']);

$this->Html->addCrumb('<i class="fa fa-dashboard"></i> Dashboard', ['controller' => 'Dashboards'], ['escape' => false]);
$this->Html->addCrumb('<i class="fa fa-question-circle"></i> List Helper', null);
?>

<style>
    .help-content{
        line-height: 1.6;
        color: #6c7d8e;
        font-size: 1.1rem;
        font-family: "Open Sans",sans-serif;
    } 
        
    
</style>

<section class="container-fluid">    
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-body"> 

                    <div class="row">

                        <div class="col-md-10 col-md-offset-1">

                            <ul id="basics" class="list-unstyled">
                                <li>
                                    <span style="color:#bbbbc7"><h3>Basics</h3></span>
                                </li>
                                <li>                           

                                    <button type="button" class="btn btn-info btn-block btn-lg" data-toggle="collapse" data-target="#help01">
                                        <span class="pull-left">What is system control for application ?</span> <i class="fa fa-angle-down pull-right"></i> 
                                    </button>
                                    <div  id="help01" class="collapse help-content">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem blanditiis delectus corporis, possimus officia sint sequi ex tenetur id impedit est pariatur iure animi non a ratione reiciendis nihil sed consequatur atque repellendus fugit perspiciatis rerum et. Dolorum consequuntur fugit deleniti, soluta fuga nobis. Ducimus blanditiis velit sit iste delectus obcaecati debitis omnis, assumenda accusamus cumque perferendis eos aut quidem! Aut, totam rerum, cupiditate quae aperiam voluptas rem inventore quas, ex maxime culpa nam soluta labore at amet nihil laborum? Explicabo numquam, sit fugit, voluptatem autem atque quis quam voluptate fugiat earum rem hic, reprehenderit quaerat tempore at. Aperiam.</p>
                                    </div>
                                </li>
                                <br />

                                <li>
                                    <button type="button" class="btn btn-info btn-block btn-lg" data-toggle="collapse" data-target="#help02">
                                        <span class="pull-left">What is necessary know to use it ? </span><i class="fa fa-angle-down pull-right"></i> 
                                    </button>
                                    <div  id="help02" class="collapse help-content">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem blanditiis delectus corporis, possimus officia sint sequi ex tenetur id impedit est pariatur iure animi non a ratione reiciendis nihil sed consequatur atque repellendus fugit perspiciatis rerum et. Dolorum consequuntur fugit deleniti, soluta fuga nobis. Ducimus blanditiis velit sit iste delectus obcaecati debitis omnis, assumenda accusamus cumque perferendis eos aut quidem! Aut, totam rerum, cupiditate quae aperiam voluptas rem inventore quas, ex maxime culpa nam soluta labore at amet nihil laborum? Explicabo numquam, sit fugit, voluptatem autem atque quis quam voluptate fugiat earum rem hic, reprehenderit quaerat tempore at. Aperiam.</p>
                                    </div>
                                </li>

                                <br />

                                <li>
                                    <button type="button" class="btn btn-info btn-block btn-lg" data-toggle="collapse" data-target="#help03">
                                        <span class="pull-left">Is it possible built application and after user system control ? </span><i class="fa fa-angle-down pull-right"></i> 
                                    </button>
                                    <div  id="help03" class="collapse help-content">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem blanditiis delectus corporis, possimus officia sint sequi ex tenetur id impedit est pariatur iure animi non a ratione reiciendis nihil sed consequatur atque repellendus fugit perspiciatis rerum et. Dolorum consequuntur fugit deleniti, soluta fuga nobis. Ducimus blanditiis velit sit iste delectus obcaecati debitis omnis, assumenda accusamus cumque perferendis eos aut quidem! Aut, totam rerum, cupiditate quae aperiam voluptas rem inventore quas, ex maxime culpa nam soluta labore at amet nihil laborum? Explicabo numquam, sit fugit, voluptatem autem atque quis quam voluptate fugiat earum rem hic, reprehenderit quaerat tempore at. Aperiam.</p>
                                    </div>
                                </li>
                                <br />

                                <li>
                                    <button type="button" class="btn btn-info btn-block btn-lg" data-toggle="collapse" data-target="#help04">
                                        <span class="pull-left">Do I need to pay for use it? </span><i class="fa fa-angle-down pull-right"></i> 
                                    </button>
                                    <div  id="help04" class="collapse help-content">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem blanditiis delectus corporis, possimus officia sint sequi ex tenetur id impedit est pariatur iure animi non a ratione reiciendis nihil sed consequatur atque repellendus fugit perspiciatis rerum et. Dolorum consequuntur fugit deleniti, soluta fuga nobis. Ducimus blanditiis velit sit iste delectus obcaecati debitis omnis, assumenda accusamus cumque perferendis eos aut quidem! Aut, totam rerum, cupiditate quae aperiam voluptas rem inventore quas, ex maxime culpa nam soluta labore at amet nihil laborum? Explicabo numquam, sit fugit, voluptatem autem atque quis quam voluptate fugiat earum rem hic, reprehenderit quaerat tempore at. Aperiam.</p>
                                    </div>
                                </li>
                            </ul> <!-- cd-faq-group -->

                            <ul id="pluginAclManager" class="list-unstyled">
                                <li>                            
                                    <span style="color:#bbbbc7"><h3>Plugin Acl Manager</h3></span>
                                </li>
                                <li>
                                    <button type="button" class="btn btn-info btn-block btn-lg" data-toggle="collapse" data-target="#help05">
                                        <span class="pull-left">What is the Plugin Acl Manager? </span><i class="fa fa-angle-down pull-right"></i> 
                                    </button>
                                    <div  id="help05" class="collapse help-content">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem blanditiis delectus corporis, possimus officia sint sequi ex tenetur id impedit est pariatur iure animi non a ratione reiciendis nihil sed consequatur atque repellendus fugit perspiciatis rerum et. Dolorum consequuntur fugit deleniti, soluta fuga nobis. Ducimus blanditiis velit sit iste delectus obcaecati debitis omnis, assumenda accusamus cumque perferendis eos aut quidem! Aut, totam rerum, cupiditate quae aperiam voluptas rem inventore quas, ex maxime culpa nam soluta labore at amet nihil laborum? Explicabo numquam, sit fugit, voluptatem autem atque quis quam voluptate fugiat earum rem hic, reprehenderit quaerat tempore at. Aperiam.</p>
                                    </div>
                                </li>
                                <br />

                                <li>
                                    <button type="button" class="btn btn-info btn-block btn-lg" data-toggle="collapse" data-target="#help06">
                                        <span class="pull-left">How do I install Plugin Acl Manager? </span><i class="fa fa-angle-down pull-right"></i> 
                                    </button>
                                    <div  id="help06" class="collapse help-content">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem blanditiis delectus corporis, possimus officia sint sequi ex tenetur id impedit est pariatur iure animi non a ratione reiciendis nihil sed consequatur atque repellendus fugit perspiciatis rerum et. Dolorum consequuntur fugit deleniti, soluta fuga nobis. Ducimus blanditiis velit sit iste delectus obcaecati debitis omnis, assumenda accusamus cumque perferendis eos aut quidem! Aut, totam rerum, cupiditate quae aperiam voluptas rem inventore quas, ex maxime culpa nam soluta labore at amet nihil laborum? Explicabo numquam, sit fugit, voluptatem autem atque quis quam voluptate fugiat earum rem hic, reprehenderit quaerat tempore at. Aperiam.</p>
                                    </div>
                                </li>
                                <br />

                                <li>
                                    <button type="button" class="btn btn-info btn-block btn-lg" data-toggle="collapse" data-target="#help07">
                                        <span class="pull-left">How do I use the Plugin Acl Manager? </span><i class="fa fa-angle-down pull-right"></i> 
                                    </button>
                                    <div  id="help07" class="collapse help-content">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem blanditiis delectus corporis, possimus officia sint sequi ex tenetur id impedit est pariatur iure animi non a ratione reiciendis nihil sed consequatur atque repellendus fugit perspiciatis rerum et. Dolorum consequuntur fugit deleniti, soluta fuga nobis. Ducimus blanditiis velit sit iste delectus obcaecati debitis omnis, assumenda accusamus cumque perferendis eos aut quidem! Aut, totam rerum, cupiditate quae aperiam voluptas rem inventore quas, ex maxime culpa nam soluta labore at amet nihil laborum? Explicabo numquam, sit fugit, voluptatem autem atque quis quam voluptate fugiat earum rem hic, reprehenderit quaerat tempore at. Aperiam.</p>
                                    </div>
                                </li>
                            </ul> 



                            <ul id="pluginAcl" class="list-unstyled">
                                <li>                            
                                    <span style="color:#bbbbc7"><h3>Plugin Acl</h3></span>
                                </li>			
                                <li>
                                    <button type="button" class="btn btn-info btn-block btn-lg" data-toggle="collapse" data-target="#help08">
                                        <span class="pull-left">What is the Plugin Acl? </span><i class="fa fa-angle-down pull-right"></i> 
                                    </button>
                                    <div  id="help08" class="collapse help-content">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem blanditiis delectus corporis, possimus officia sint sequi ex tenetur id impedit est pariatur iure animi non a ratione reiciendis nihil sed consequatur atque repellendus fugit perspiciatis rerum et. Dolorum consequuntur fugit deleniti, soluta fuga nobis. Ducimus blanditiis velit sit iste delectus obcaecati debitis omnis, assumenda accusamus cumque perferendis eos aut quidem! Aut, totam rerum, cupiditate quae aperiam voluptas rem inventore quas, ex maxime culpa nam soluta labore at amet nihil laborum? Explicabo numquam, sit fugit, voluptatem autem atque quis quam voluptate fugiat earum rem hic, reprehenderit quaerat tempore at. Aperiam.</p>
                                    </div>
                                </li>
                                <br />

                                <li>
                                    <button type="button" class="btn btn-info btn-block btn-lg" data-toggle="collapse" data-target="#help09">
                                        <span class="pull-left">How do I install Plugin Acl? </span><i class="fa fa-angle-down pull-right"></i> 
                                    </button>
                                    <div  id="help09" class="collapse help-content">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem blanditiis delectus corporis, possimus officia sint sequi ex tenetur id impedit est pariatur iure animi non a ratione reiciendis nihil sed consequatur atque repellendus fugit perspiciatis rerum et. Dolorum consequuntur fugit deleniti, soluta fuga nobis. Ducimus blanditiis velit sit iste delectus obcaecati debitis omnis, assumenda accusamus cumque perferendis eos aut quidem! Aut, totam rerum, cupiditate quae aperiam voluptas rem inventore quas, ex maxime culpa nam soluta labore at amet nihil laborum? Explicabo numquam, sit fugit, voluptatem autem atque quis quam voluptate fugiat earum rem hic, reprehenderit quaerat tempore at. Aperiam.</p>
                                    </div>
                                </li>
                                <br />

                                <li>
                                    <button type="button" class="btn btn-info btn-block btn-lg" data-toggle="collapse" data-target="#help10">
                                        <span class="pull-left">How do I use the Plugin Acl? </span><i class="fa fa-angle-down pull-right"></i> 
                                    </button>
                                    <div  id="help10" class="collapse help-content">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem blanditiis delectus corporis, possimus officia sint sequi ex tenetur id impedit est pariatur iure animi non a ratione reiciendis nihil sed consequatur atque repellendus fugit perspiciatis rerum et. Dolorum consequuntur fugit deleniti, soluta fuga nobis. Ducimus blanditiis velit sit iste delectus obcaecati debitis omnis, assumenda accusamus cumque perferendis eos aut quidem! Aut, totam rerum, cupiditate quae aperiam voluptas rem inventore quas, ex maxime culpa nam soluta labore at amet nihil laborum? Explicabo numquam, sit fugit, voluptatem autem atque quis quam voluptate fugiat earum rem hic, reprehenderit quaerat tempore at. Aperiam.</p>
                                    </div>
                                </li>
                            </ul>

                        </div>
                    </div>




                </div>

                <div class="box-footer">                    
                    <div> <br /><br /> </div>
                </div>                
            </div>
        </div>
    </div>
</section>   
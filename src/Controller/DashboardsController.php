<?php
namespace AclManager\Controller;

use AclManager\Controller\AppController;
use Cake\Core\Plugin;
// use Cake\Database\Schema\Collection;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;

use Acl\AclExtras;
use Cake\Core\Configure;


/**
 * Dashboards Controller
 *
 * @property \AclManager\Controller\Dashboards
 */
class DashboardsController extends AppController
{

    public function initialize() {
        
        parent::initialize();
        
        if( Plugin::loaded('Acl') && ! $this->Acl ){
            $this->allow('index');
        }
        
    }
    
    /**
     * beforeRender method
     *
     * @return void
     */    
    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
        
    }    

    /**
     * Index method
     *
     * @return void
     */
    public function index( $parameter = NULL )
    {        
        // PARAMETERS TO BE SENT FOR VIEW.
        $mainSystem      = NULL;
        $aclPluginLoaded = FALSE;
        $aclTables       = [];
		
        // DATABASE NAME
        $aclDatabase = Configure::read('Acl.database');                
        
        if( $aclDatabase ){
                // CREATE DB CONNECTION TO BD DEFAULT        
                $connection = ConnectionManager::get($aclDatabase);

                // CREATE A SCHEMA COLLECTION.
                $collection = $connection->schemaCollection();

                // INFO ABOUT MAIN SYSTEM                
                $session    = $this->request->session();        
                $mainSystem = $session->read('mainSystemInfo');

                // PLUGIN ACL LOADED.        
                $aclPluginLoaded = Plugin::loaded('Acl');

                // Get the table names
                $aclTables = $collection->listTables();        
        }
                
        // VARIABLES PASS TO VIEW.
        $this->set(compact('mainSystem','aclPluginLoaded', 'aclTables', 'aclDatabase' ));
        
        if( \trim($parameter) === 'acoSyncronize' ){
            $this->acoSyncronize();
        }
    }
    
    public function acoSyncronize(){
        
        $acl_extra = new AclExtras();
        
        $acl_extra->startup();
        $acl_extra->controller->Flash = $this->Flash;
        
        $acl_extra->Shell = null;
                
        $acl_extra->acoSync();
        
        
    }
    

}

<?php

namespace AclManager\Controller;

use AclManager\Controller\AppController;

use Cake\Event\Event;
use Cake\Core\Configure;

use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Users Controller
 * 
 */
class UsersController extends AppController {

    public $model;
    public $aliasField;
    public $table;
    public $AclManagerAuxTable;
    
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
    }

    public function beforeRender(Event $event) {
        parent::beforeRender($event);
        
    }

    /**
     * Index method
     *
     * @return void
     * @return void
     */
    public function index($option = 'all') {        
        
        $this->loadModel('Aros');   
        
        // SEARCH ALL GROUPS
        $groups = $this->Aros->find( 'list', [ 'keyField' => 'id','valueField' => 'alias'])
                             ->where( ['parent_id IS NULL'] )->toArray();
        
        $search = !empty($this->request->data['search']) ? $this->request->data['search'] : NULL;
                
        $conditions = $this->search($search);
        
        // WHEREAS ALL REGISTER WITH PARENT_ID NOT NULL AND A USER
        $this->set( 'users', 
                    $this->paginate( $this->Aros->find('all')->where( ['parent_id IS NOT NULL', $conditions ])));
                       
        $this->set('search', $search);
        $this->set('groups', $groups);
        
    }

    /**
      Metodo para formatar a consulta.
     */
    function search($search) {

        if (!$search)
            return NULL;
        
        // NOME DO USUARIO
        $result['alias LIKE'] = "%" . mb_strtoupper($search['name']) . "%";

        return $result;
    }


    /**
     * View method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->loadModel('Aros');
        
        $user = $this->Aros->get($id);
        
        // SEARCH ALL GROUPS
        $groups = $this->Aros->find( 'list', [ 'keyField' => 'id','valueField' => 'alias'])
                             ->where( ['parent_id IS NULL'] )->toArray();
        
        $this->set('user', $user);
        $this->set('groups', $groups);        
    }

    /**
     * Add method
     *
     * @return void
     */
    public function add()
    {        
        $this->loadModel('Aros');
        
        // SEARCH ALL GROUPS
        $groups = $this->Aros->find( 'list', [ 'keyField' => 'id','valueField' => 'alias'])
                             ->where( ['parent_id IS NULL'] )->toArray();
        
        $user = $this->Aros->newEntity();
                                           
        if ( $this->request->is('post')  ) {            
            
            $data = $this->request->data;        
            $tableName  = $data['model'];            
            $foreingKey = $data['foreign_key']; 
            
                        
            // IF VALIDATE...
            if( $this->validate($data, $tableName, $foreingKey, $groups) ){
            
                $user = $this->Aros->patchEntity($user, $this->request->data);            
            
                $user['alias']     = strtoupper( trim( $user['alias']) );                
                        
                if ($this->Aros->save($user)) {                
                    $this->Flash->success(__('The user {0} has been saved.', $user->alias));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The user {0} could not be saved. Please, try again.', $user->alias ));
                }
            }
        }   
        
        $this->set(compact('user', 'groups'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        
        $this->loadModel('Aros');
        
         // SEARCH ALL GROUPS
        $groups = $this->Aros->find( 'list', [ 'keyField' => 'id','valueField' => 'alias'])
                             ->where( ['parent_id IS NULL'] )->toArray();
        
        $user = $this->Aros->get( $id );
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $data = $this->request->data;            
            $tableName  = $data['model'];            
            $foreingKey = $data['foreign_key']; 
                                   
            // IF VALIDATE...
            if( $this->validate($data, $tableName, $foreingKey, $groups) ){

                $user = $this->Aros->patchEntity($user, $data );
                $user['alias'] = strtoupper( trim($user['alias']) );
                
                if ( $this->Aros->save($user) ) {
                    $this->Flash->success(__('The user {0} has been saved.', $user->alias ));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The user {0} could not be saved. Please, try again.', $user->alias ));
                }
            }
        }
        $this->set(compact('groups', 'user'));        
    }

    /**
     * Delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {        
       
        $this->loadModel('Aros');
        
        $user = $this->Aros->get($id);  

        if ($this->Aros->delete($user)) {
            $this->Flash->success(__('The user {0} has been deleted.', $user->name));
        } else {
            $this->Flash->error(__('The user {0} could not be deleted. Please, try again.', $user->name));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    /**
     * Validate method
     *
     * @data request post send form. 
     * @tableName name of the table used with GROUP TABLE.
     * @return TRUE OR FALSE     
     */
    private function validate( $data, $tableName, $foreingKey, $groups) {
        
        if( trim($data['alias']) == '' ){
            $this->Flash->error(__('Please inform a name for user (Alias).'));
            return FALSE;
        }
        
        // IT'S NECESSARY CHOOSE A GROUP FOR PUT USER.
        if( !isset($data['parent_id'] ) || empty($data['parent_id']) ){
            $this->Flash->error(__('It is necessary inform GROUP for user. Please, try again.'));
            return FALSE;
        }
        
        $grupoId = $data['parent_id'];
        
        if( ! array_key_exists( $grupoId, $groups ) ){
            $this->Flash->error(__('Sorry not exist none GROUP with groupId informated. Please, try again.'));
            return FALSE;
        } 

        // IF NOT SET MODEL AND FOREIGN_KEY RETURN TRUE
        if( ( !isset($data['model']) || empty($data['model'])) &&
            ( !isset($data['foreign_key']) || empty($data['foreign_key']))  ) {
            
            return TRUE;
        }
        
        // IF SET MODEL BUT NOT SET FOREIGN KEY RETURN FALSE
        if( ( isset($data['model']) && ! empty($data['model'])) &&
            ( !isset($data['foreign_key']) || empty($data['foreign_key']))  ) {
            
            $this->Flash->error(__('It is necessary inform FOREIGN KEY. Please, try again.'));
            return FALSE;
        }
        
        // IF SET FOREIGN KEY BUT MODEL NOT SET  RETURN FALSE
        if( ( ! isset($data['model']) ||  empty($data['model'])) &&
            ( isset($data['foreign_key']) && ! empty($data['foreign_key']))  ) {
            
            $this->Flash->error(__('It is necessary inform MODEL. Please, try again.'));
            return FALSE;
        }
        
        // GET CONNECTION.
        $connection = Configure::read('Acl.database');
                
        // GET DATABASE
        $db = ConnectionManager::get($connection);
                
        // GET TABLES' NAME.
        $collection = $db->schemaCollection();                
        $tables = $collection->listTables();
                
        if( ! in_array($tableName, $tables ) ){
            $this->Flash->error(__("I'm sorry not found table {0} into schema {1}. Please check your database and/or Acl config database (Acl.database).", $tableName, $connection));
            return FALSE;
        }
        
        // REGISTER TABLE $TableName
        if ( TableRegistry::exists($tableName) ) {
                $table = TableRegistry::get($tableName);
        } else {                
            $table = TableRegistry::get( $tableName, [
                                         'connection' => ConnectionManager::get($connection)
                                        ]);
        }
        
        // CHECK IF TABLE HAVE PRIMARY KEY
        $primaryKey = $table->primaryKey();                        
        
        if( empty($primaryKey) ){
            $this->Flash->error(__("It is necessary define primary key in your table {0}. Please check your database and/or Acl config database (Acl.database).", $tableName));
            return FALSE;
        }
        
        // CHECK IF TABLENAME/FOREIGN KEY EXIST .        
        $result = $table->find('all')->where( [ $primaryKey => $foreingKey] )->count();
        if( $result <= 0 ){
            $this->Flash->error(__("Not exist none register into table {0} with {1} which value is {2}. Please check your table/registers", $tableName, $primaryKey, $foreingKey ));
            return FALSE;
        }
        return TRUE;
    }

}

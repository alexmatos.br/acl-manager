<?php

namespace AclManager\Controller;

use App\Controller\AppController as BaseController;
use Cake\ORM\TableRegistry;
use Cake\Core\Plugin;

class AppController extends BaseController
{       
    // HERE IS THE HELPERS USED FOR PLUGIN ACLMANAGER.
    public $helpers = [ 'AclManager.PluginAcl','AclManager.PluginPageHeader' ];
	
       
    public function initialize() {
                           
        parent::initialize();		
        
        if( Plugin::loaded('Acl') && ! $this->Acl ){
            $this->loadComponent('Acl.Acl');
        }
              

        if( ! $this->isAuthorized() ){
             $this->Flash->error(__('You are not authorized to access that location.'));   
             return $this->redirect( ['controller' => 'Dashboards', 'action' => 'index']);             
        }
        
        
        
    }    
    
    public function isAuthorized() {
       
	
        // SET VARIABLES...          
        $plugin     = isset( $this->request->plugin )     ? $this->request->plugin     : NULL;
        $controller = isset( $this->request->controller ) ? $this->request->controller : NULL;
        $action     = isset( $this->request->action )     ? $this->request->action     : NULL; 
        
        // SET VARIABLE IF PLUGIN ACL LOADED.
        $aclPluginLoaded = Plugin::loaded('Acl');
        
        // WHITHOUT CONTROLLER AND ACTION RETURN TRUE;
        if( ! $controller && ! $action ){
            return true;
        }
        
        // ALL USER CAN ACCESS DASHBOARD OF THE AclManager.
        if( $controller === 'Dashboards' && $action === 'index' ){
            return true;
        }
        
        // ALL USER CAN ACCESS HELPERS OF THE AclManager.
        if( $controller === 'Helpers' && ( $action === 'index' || $action == 'getHelper') ){
            return true;
        }        
        
        // INFO ABOUT MAIN SYSTEM                
        $session    = $this->request->session();        
        $mainSystem = $session->read('mainSystemInfo'); 

        // AUTHENTING THE USER ...
        if( empty($mainSystem) || ! isset( $mainSystem['userProfile'] ) ){
            
            $this->Flash->error(__('You have set session variable <b>mainSystemInfo</b>, see mainSystem parameters.'));   
            
            return false;
        }                
                               
                
        // IF NOT LOADED PLUGIN ACL RETURN
        if( ! $aclPluginLoaded ){
            
            $this->Flash->error(__('You have install Plugin Acl to access Plugin Manager Acl.'));   
            
            return false;
        } 

        
        // VERIFY IF USER HAS PERMISSION IN THE CONTROLLER/ACTION
	// OBS. THE PLUGIN ACL USE THE SLASH "/".
        foreach ( $mainSystem['userProfile'] as $profile ) { 
            
            if ( @$this->Acl->check($profile, "$plugin/$controller/$action" ) ){
                return true;
            }    
        }

        return false;
    }    

}
